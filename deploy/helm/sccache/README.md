Create cluster
```bash
openstack coe cluster create --cluster-template kubernetes-1.25.3-3 \
    --keypair dtomasgu-key \
    --node-count 1 \
    --flavor m2.large
    --merge-labels \
    --labels monitoring_enabled=true \
    --labels logging_producer=kubernetes \
    --labels cert_manager_io_enabled=true \
    --labels auto_scaling_enabled=true \
    --labels min_node_count=1 \
    --labels max_node_count=10 \
    --labels ingress_controller=nginx \
    distributed-sccache-001

openstack coe nodegroup create \
    --node-count 5 \
    --min-nodes 5 \
    --max-nodes 10 \
    --flavor m2.2xlarge \
    --role build-server \
    distributed-sccache-001 2xlarge
```
---

Create service frontend for scheduler
```bash
export OS_REGION_NAME=sdn1
openstack loadbalancer create --name distributed-sccache --vip-network-id public
openstack loadbalancer set --description sccache,distributed,distributed-sccache distributed-sccache

openstack loadbalancer listener create --name distributed-sccache-https \
                                       --protocol TCP \
                                       --protocol-port 443 distributed-sccache
openstack loadbalancer listener create --name distributed-sccache-http \
                                       --protocol HTTP \
                                       --protocol-port 80 distributed-sccache

openstack loadbalancer pool create --name distributed-sccache-https  \
                                  --lb-algorithm ROUND_ROBIN \
                                  --listener distributed-sccache-https \
                                  --protocol TCP
openstack loadbalancer pool create --name distributed-sccache-http  \
                                  --lb-algorithm ROUND_ROBIN \
                                  --listener distributed-sccache-http \
                                  --protocol HTTP

# TODO:
# openstack loadbalancer healthmonitor create --name infra-staging-registry-https-monitor \
#                                             --delay 7 \
#                                             --timeout 5 \
#                                             --max-retries 3 \
#                                             --url-path /api/v2.0/health \
#                                             --expected-codes 200 \
#                                             --type HTTPS infra-staging-registry-https
# openstack loadbalancer healthmonitor create --name infra-staging-registry-http-monitor \
#                                             --delay 7 \
#                                             --timeout 5 \
#                                             --max-retries 3 \
#                                             --url-path /api/v2.0/health \
#                                             --expected-codes 308,200 \
#                                             --type HTTP infra-staging-registry-http
 
kubectl label no -l magnum.openstack.org/role=worker role=ingress
 
kubectl get no -owide -l role=ingress | grep -v NAME | awk {'print $1 " " $6'} | xargs -n 2 sh -c 'openstack loadbalancer member create --protocol-port 443 --address $1 --name $0 distributed-sccache-https; openstack loadbalancer member create --protocol-port 80 --address $1 --name $0 distributed-sccache-http'
 
for node in `kubectl get no -l role=ingress -o jsonpath="{.items[*].metadata.name}"`; do
    openstack server set --property landb-set="KUBERNETES-ACME" $node
done
```
---

Patch the cert-manager clusterissuers:
```bash
kubectl patch clusterissuer -n kube-system letsencrypt-staging --type "json" --patch  '[{"op":"add", "path":"/spec/acme/email", "value":"kubernetes-3rd-level@cern.ch"}]'
kubectl patch clusterissuer -n kube-system letsencrypt --type "json" --patch  '[{"op":"add", "path":"/spec/acme/email", "value":"kubernetes-3rd-level@cern.ch"}]'
```

Download and setup sccache client. Create configuration file as follows:
```bash
cat ~/.config/sccache/config
[dist]
scheduler_url = "https://sccache.cern.ch"
# Used for mapping local toolchains to remote cross-compile toolchains. Empty in
# this example where the client and build server are both Linux.
toolchains = []
# Size of the local toolchain cache, in bytes (5GB here, 10GB if unspecified).
toolchain_cache_size = 5368709120

[dist.auth]
type = "oauth2_implicit"
client_id = "sccache"
auth_url = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth?scope=openid"

#[cache.s3]
#bucket = "sccache"
#endpoint = "https://s3.cern.ch/"
#use_ssl = true
#key_prefix = "test"
#no_credentials = false

# Bucket access credentials see: https://github.com/mozilla/sccache/blob/main/docs/S3.md#credentials
# AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY
# OR ~/.aws/credentials ~/.aws/config
```

Login and get stats:
```
sccache --dist-auth
sccache --dist-status 
{"SchedulerStatus":["https://sccache.cern.ch/",{"num_servers":5,"num_cpus":80,"in_progress":0}]}
```


# Compiling CERN workloads
## Root:
```
For root, follow the steps here: https://root.cern/install/build_from_source/

cmake -DCMAKE_C_COMPILER_LAUNCHER=sccache -DCMAKE_CXX_COMPILER_LAUNCHER=sccache ../root
cmake --build . --target install -j `sccache --dist-status | jq '.SchedulerStatus[1].num_cpus' -r`
```

## FESA:
```
From within the VM run:

# Binary needed to be added manually as user has no root priviledges
make CC='~/.local/bin/sccache gcc' CXX='~/.local/bin/sccache g++' -j `sccache --dist-status | jq '.SchedulerStatus[1].num_cpus' -r`
```