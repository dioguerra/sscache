apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "sccache.fullname" . }}-server
  labels:
    {{- include "sccache.labels" . | nindent 4 }}
spec:
  {{- if not .Values.server.autoscaling.enabled }}
  replicas: {{ .Values.server.replicaCount }}
  {{- end }}
  strategy:
    type: Recreate
  selector:
    matchLabels:
      {{- include "sccache.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: server
  template:
    metadata:
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
      {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "sccache.selectorLabels" . | nindent 8 }}
        app.kubernetes.io/component: server
    spec:
      hostNetwork: true
      hostIPC: true
      hostPID: true
      dnsPolicy: ClusterFirstWithHostNet
      {{- with .Values.server.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "sccache.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      initContainers:
      - image: registry.cern.ch/kubernetes/ops:0.1.0
        name: selinux-disable
        command: ["/bin/bash", "-c", "setenforce 0"]
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /usr/sbin
          name: usrsbin
        - mountPath: /dev
          name: dev
        - mountPath: /etc
          name: etc
        - mountPath: /var/run
          name: varrun
        - mountPath: /sys
          name: sys
      - name: overwrite-server-ip
        image: registry.cern.ch/kubernetes/busybox:1.36.1
        command:
        - /bin/sh
        - -c
        - |
          cp /tmp/config/server.conf /data/config/server.conf
          sed -i -e "s/^public_addr = \".*\"/public_addr = \"$HOST_IP:{{ .Values.server.service.port }}\"/g" /data/config/server.conf
          chmod 440 /data/config/server.conf
        env:
        - name: HOST_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        volumeMounts:
        - name: temp
          mountPath: /tmp/config
        - name: config
          mountPath: /data/config
      containers:
      - name: {{ .Chart.Name }}
        securityContext:
          {{- toYaml .Values.securityContext | nindent 12 }}
        image: "{{ .Values.server.image.repository }}:{{ .Values.server.image.tag | default .Chart.AppVersion }}"
        imagePullPolicy: {{ .Values.server.image.pullPolicy }}
        args:
        - server
        - --config
        - /data/config/server.conf
        env:
        - name: SCCACHE_LOG
          value: "info"
        {{- range $key, $val := .Values.env }}
        - name: {{ $key }}
          value: {{ $val | quote }}
        {{- end}}
        ports:
        - name: http
          containerPort: {{ .Values.server.service.port }}
          protocol: TCP
        # livenessProbe:
        #   httpGet:
        #     path: /
        #     port: http
        # readinessProbe:
        #   httpGet:
        #     path: /
        #     port: http
        resources:
          {{- toYaml .Values.resources | nindent 12 }}
        securityContext:
          capabilities:
            # TODO: Try this for now. check how we can create usernamespaces from unpriviledged account after.
            # https://kubernetes.io/docs/concepts/workloads/pods/user-namespaces/
            # Error from server side: Res 563 error: run build failed, caused by: Compilation execution failed, caused by: Failed to enter a new Linux namespace, caused by: EPERM: Operation not permitted
            add:
            - CAP_SYS_ADMIN
          runAsUser: 0
          runAsGroup: 0
          privileged: true
        volumeMounts:
        - name: config
          mountPath: /data/config
        - mountPath: /usr/sbin
          name: usrsbin
        - mountPath: /dev
          name: dev
        - mountPath: /etc
          name: etc
        - mountPath: /var/run
          name: varrun
        - mountPath: /sys
          name: sys
        - mountPath: /tmp
          name: tmp
      volumes:
      - name: temp
        secret:
          secretName: {{ include "sccache.fullname" . }}-config
          defaultMode: 444
      - name: config
        emptyDir: {}
      - hostPath:
          path: /dev
          type: ""
        name: dev
      - hostPath:
          path: /usr/sbin
          type: ""
        name: usrsbin
      - hostPath:
          path: /sys
          type: ""
        name: sys
      - hostPath:
          path: /var/run
          type: ""
        name: varrun
      - hostPath:
          path: /etc
          type: ""
        name: etc
      - hostPath:
          path: /tmp
          type: ""
        name: tmp
      {{- with .Values.server.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app.kubernetes.io/name
                operator: In
                values:
                - {{ include "sccache.name" . }}
            topologyKey: "kubernetes.io/hostname"
        {{- with .Values.affinity }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
